import 'package:flutter/material.dart';

/// Useful for simple busy state management. Call [busy] and pass in true or false
/// to set, and check whether or not it is currently busy by calling [busy] with
/// no params.
mixin BusyNotifierMixin on ChangeNotifier {
  dynamic _busy = false;
  get busy => _busy;
  set busy(dynamic busy) {
    _busy = busy;
    notifyListeners();
  }
}
