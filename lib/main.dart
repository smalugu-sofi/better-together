import 'package:better_together/data/timeline_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/svg.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'data/timeline_vm.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Better together',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: EngagementScreen(),
    );
  }
}

class EngagementScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset(
              'sofi-jewel-turquoise.svg',
              height: 140,
              width: 140,
            ),
            onPressed: () {},
          ),
          title: Text(
            'BetterTogether',
            style: Theme.of(context).textTheme.headline2,
          ),
          backgroundColor: Colors.white,
        ),
        body: ChangeNotifierProvider(
            create: (context) => TimelineViewModel()..fetchTimeLine(),
            child:
                Consumer<TimelineViewModel>(builder: (context, model, child) {
              if (model.busy) {
                return Center(child: CircularProgressIndicator());
              } else {
                return Padding(
                  padding: const EdgeInsets.only(
                        left: 24, right: 24, top: 16, bottom: 24),
                  child: SingleChildScrollView(
                    child: Column(
                        children: [
                          Column(
                            children: [
                              Text(
                                'Meeting id: ${model.meeting.id}',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 20),
                              Text(
                                'Meeting topic: ${model.meeting.topic}',
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                              SizedBox(height: 20),
                              _ShowState(
                                  model.engagementScore,
                                  model.engagementString,
                                  model.engagementColor),
                              SizedBox(height: 20),
                              SvgPicture.asset(
                                'engagement_breakdown.svg',
                                height: 40,
                                width: 320,
                              ),
                            ],
                          ),
                          SizedBox(height: 32),
                          const Divider(
                            height: 20,
                            thickness: 5,
                            indent: 20,
                            endIndent: 20,
                          ),
                          SizedBox(height: 20),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Participation Breakdown',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontWeight: FontWeight.bold)),
                                Text('By number of seconds',
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                                SizedBox(
                                  height: 300.0,
                                  child: SimpleBarChart(
                                    model.usersCharts,
                                    animate: true,
                                  ),
                                ),
                                SizedBox(height: 30),
                                Text('Meeting insights',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(fontWeight: FontWeight.bold)),
                                SizedBox(height: 10),
                                Text(
                                    'You’re meeting has great engagement. High five!'),
                                SizedBox(height: 10),
                                Text(
                                    'You were in the top 90% of speakers. Way to spread the love!'),
                                SizedBox(height: 20),
                                Text(
                                    'Want to learn how to run and participate in more inclusive meetings?',
                                    style:
                                        Theme.of(context).textTheme.headline5),
                                Row(children: [
                                  Image(
                                      image: AssetImage('card_article1.png'),
                                      height: 296,
                                      width: 262),
                                  Image(
                                      image: AssetImage('card_article2.png'),
                                      height: 296,
                                      width: 262),
                                ])
                              ]),
                        ],
                      ),
                  ),
                );
              }
            })));
  }
}

Widget _ShowState(String score, String string, MaterialColor color) {
  return Column(
    children: [
      Text('$score %', style: TextStyle(color: color, fontSize: 40)),
      Text('$string', style: TextStyle(color: color, fontSize: 24))
    ],
  );
}

class SimpleBarChart extends StatelessWidget {
  final List<charts.Series<dynamic, String>> seriesList;
  final bool animate;

  SimpleBarChart(this.seriesList, {required this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory SimpleBarChart.withSampleData() {
    return new SimpleBarChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      vertical: false,
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<ActiveUsers, String>> _createSampleData() {
    final data = [
      new ActiveUsers(
          avatarUrl: '',
          userId: 2,
          username: 'SJ',
          activeTime: Duration(seconds: 23)),
      new ActiveUsers(
          avatarUrl: '',
          userId: 6,
          username: 'Sr',
          activeTime: Duration(seconds: 34)),
      new ActiveUsers(
          avatarUrl: '',
          userId: 9,
          username: 'SK',
          activeTime: Duration(seconds: 90)),
    ];

    return [
      new charts.Series<ActiveUsers, String>(
        id: 'Times',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (ActiveUsers user, _) => user.username,
        measureFn: (ActiveUsers user, _) => user.userId,
        data: data,
        labelAccessorFn: (ActiveUsers user, _) => '${user.activeTime}}',
      )
    ];
  }
}
