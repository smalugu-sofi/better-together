import 'package:json_annotation/json_annotation.dart';
import 'package:quiver/core.dart';

part 'timeline_model.g.dart';

@JsonSerializable()
class Timeline {
  late List<TimelineElement> timeline;

  Timeline({
    required this.timeline,
  });

  Timeline.fromJson(Map<String, dynamic> json) {
    timeline = List<dynamic>.from(json['timeline'])
        .map((e) => TimelineElement.fromJson(e))
        .toList();
  }
  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['timeline'] = timeline.map((e) => e.toJson()).toList();
    return _data;
  }
}

@JsonSerializable()
class TimelineElement {
  late String ts;
  List<ActiveUsers> activeUsers = [];

  TimelineElement({
    required this.ts,
    this.activeUsers = const [],
  });

  TimelineElement.fromJson(Map<String, dynamic> json) {
    ts = json['ts'];
    if (List<dynamic>.from(json['users']).isNotEmpty) {
      activeUsers = List<dynamic>.from(json['users']).map((e) {
        return ActiveUsers.fromJson(e);
      }).toList();
    }
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ts'] = ts;
    _data['users'] = activeUsers.map((e) => e.toJson()).toList();
    return _data;
  }
}

@JsonSerializable()
class ActiveUsers {
  late String avatarUrl;
  late int userId;
  late String username;
  late Duration activeTime;

  @override
  String toString() {
    return 'username: $username id: $userId avatar: $avatarUrl';
  }

  bool operator ==(o) => o is ActiveUsers && userId == o.userId;
  int get hashCode => hash2(userId, username);

  ActiveUsers({
    required this.avatarUrl,
    required this.userId,
    required this.username,
    this.activeTime = const Duration(seconds: 0)
  });

  ActiveUsers.fromJson(Map<String, dynamic> json) {
    avatarUrl = json['avatar_url'];
    userId = json['user_id'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['avatar_url'] = avatarUrl;
    _data['user_id'] = userId;
    _data['username'] = username;
    return _data;
  }
}
