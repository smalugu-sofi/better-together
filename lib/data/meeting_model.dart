import 'package:json_annotation/json_annotation.dart';

part 'meeting_model.g.dart';

@JsonSerializable()
class Meeting {
  late String uuid;
  late int id;
  late String accountId;
  late String hostId;
  late String topic;
  late int type;
  late String startTime;
  late String timezone;
  late String hostEmail;
  late int duration;
  late int totalSize;
  late int recordingCount;
  late String shareUrl;
  late List<RecordingFiles> recordingFiles;

  Meeting({
    required this.uuid,
    required this.id,
    required this.accountId,
    required this.hostId,
    required this.topic,
    required this.type,
    required this.startTime,
    required this.timezone,
    required this.hostEmail,
    required this.duration,
    required this.totalSize,
    required this.recordingCount,
    required this.shareUrl,
    required this.recordingFiles,
  });

  Meeting.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    id = json['id'];
    accountId = json['account_id'];
    hostId = json['host_id'];
    topic = json['topic'];
    type = json['type'];
    startTime = json['start_time'];
    timezone = json['timezone'];
    hostEmail = json['host_email'];
    duration = json['duration'];
    totalSize = json['total_size'];
    recordingCount = json['recording_count'];
    shareUrl = json['share_url'];
    recordingFiles = List<dynamic>.from(json['recording_files'])
        .map((e) => RecordingFiles.fromJson(e))
        .toList();
  }
  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['uuid'] = uuid;
    _data['id'] = id;
    _data['account_id'] = accountId;
    _data['host_id'] = hostId;
    _data['topic'] = topic;
    _data['type'] = type;
    _data['start_time'] = startTime;
    _data['timezone'] = timezone;
    _data['host_email'] = hostEmail;
    _data['duration'] = duration;
    _data['total_size'] = totalSize;
    _data['recording_count'] = recordingCount;
    _data['share_url'] = shareUrl;
    _data['recording_files'] = recordingFiles.map((e) => e.toJson()).toList();
    return _data;
  }
}

@JsonSerializable()
class RecordingFiles {
  late String id;
  late String meetingId;
  late String recordingStart;
  late String recordingEnd;
  late String fileType;
  late String fileExtension;
  late int fileSize;
  late String downloadUrl;
  late String status;
  late String recordingType;

  RecordingFiles({
    required this.id,
    required this.meetingId,
    required this.recordingStart,
    required this.recordingEnd,
    required this.fileType,
    required this.fileExtension,
    required this.fileSize,
    required this.downloadUrl,
    required this.status,
    required this.recordingType,
  });

  RecordingFiles.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    meetingId = json['meeting_id'];
    recordingStart = json['recording_start'];
    recordingEnd = json['recording_end'];
    fileType = json['file_type'];
    fileExtension = json['file_extension'];
    fileSize = json['file_size'];
    downloadUrl = json['download_url'];
    status = json['status'];
    recordingType = json['recording_type'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['meeting_id'] = meetingId;
    _data['recording_start'] = recordingStart;
    _data['recording_end'] = recordingEnd;
    _data['file_type'] = fileType;
    _data['file_extension'] = fileExtension;
    _data['file_size'] = fileSize;
    _data['download_url'] = downloadUrl;
    _data['status'] = status;
    _data['recording_type'] = recordingType;
    return _data;
  }
}
