// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meeting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Meeting _$MeetingFromJson(Map<String, dynamic> json) {
  return Meeting(
    uuid: json['uuid'] as String,
    id: json['id'] as int,
    accountId: json['accountId'] as String,
    hostId: json['hostId'] as String,
    topic: json['topic'] as String,
    type: json['type'] as int,
    startTime: json['startTime'] as String,
    timezone: json['timezone'] as String,
    hostEmail: json['hostEmail'] as String,
    duration: json['duration'] as int,
    totalSize: json['totalSize'] as int,
    recordingCount: json['recordingCount'] as int,
    shareUrl: json['shareUrl'] as String,
    recordingFiles: (json['recordingFiles'] as List<dynamic>)
        .map((e) => RecordingFiles.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MeetingToJson(Meeting instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'id': instance.id,
      'accountId': instance.accountId,
      'hostId': instance.hostId,
      'topic': instance.topic,
      'type': instance.type,
      'startTime': instance.startTime,
      'timezone': instance.timezone,
      'hostEmail': instance.hostEmail,
      'duration': instance.duration,
      'totalSize': instance.totalSize,
      'recordingCount': instance.recordingCount,
      'shareUrl': instance.shareUrl,
      'recordingFiles': instance.recordingFiles,
    };
