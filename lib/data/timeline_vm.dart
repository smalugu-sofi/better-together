import 'dart:developer';

import 'package:better_together/data/timeline_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:charts_flutter/flutter.dart' as charts;

import '../busy_notifier.dart';
import 'meeting_model.dart';

String demo = 'good';

class TimelineViewModel extends ChangeNotifier with BusyNotifierMixin {
  Map<ActiveUsers, Duration> usersActiveTimeMap = {};
  List<charts.Series<dynamic, String>> usersCharts = [];
  late Meeting meeting;
  //Doing these here in stead of widget because of dirty errors
  String engagementScore = '';
  MaterialColor engagementColor = Colors.green;
  String engagementString = 'Equitable Engagement';

  Future<void> fetchMeeting() async {
    busy = true;
    final rawString = await loadAsset('$demo-meeting.json');
    final map = fromStringToJsonMap(rawString);
    meeting = Meeting.fromJson(map);
    busy = false;
  }

  Future<Timeline> fetchTimeLine() async {
    busy = true;
    await fetchMeeting();
    final rawString = await loadAsset('$demo-meeting-transcript.json');
    final map = fromStringToJsonMap(rawString);
    final transcript = Timeline.fromJson(map);
    //Adding dummydate but can run risk of a meeting crossing 24 hours
    final dummyDate = '2021-04-27';
    final startTime = '00:00:00.000';
    var firstTime = DateTime.parse('$dummyDate $startTime');
    Duration totalTime = Duration(seconds: 0);
    DateTime? afterTime;
    if (transcript.timeline.length > 2) {
      for (TimelineElement tle in transcript.timeline) {
        afterTime = DateTime.tryParse('$dummyDate ${tle.ts}');
        Duration elapsedTime = afterTime!.difference(firstTime);
        // log('times are $afterTime and $firstTime and difference $elapsedTime');
        firstTime = afterTime;
        for (ActiveUsers user in tle.activeUsers) {
          // log('Time used so far by ${user.username} is ${usersActiveTimeMap[user]}');
          if (usersActiveTimeMap.containsKey(user)) {
            usersActiveTimeMap[user] = usersActiveTimeMap[user]! + elapsedTime;
            user.activeTime = usersActiveTimeMap[user]!;
          } else {
            user.activeTime = elapsedTime;
            usersActiveTimeMap.putIfAbsent(user, () => elapsedTime);
          }
        }
      }
    }
    for (var entries in usersActiveTimeMap.entries) {
      entries.key.activeTime = entries.value;
      totalTime += entries.value;
      log('Active speaker ${entries.key.username} with elapsed time ${entries.key.activeTime.inSeconds} with total time ${entries.value.inSeconds}');
    }
    usersCharts = [
      new charts.Series<ActiveUsers, String>(
        id: 'Active mins',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (ActiveUsers users, _) => users.username,
        measureFn: (ActiveUsers users, _) => users.activeTime.inSeconds,
        data: usersActiveTimeMap.keys.toList(),
        labelAccessorFn: (ActiveUsers user, _) =>
            '${computeAverage(user, totalTime)} %',
        insideLabelStyleAccessorFn: (ActiveUsers user, _) {
          final color = (computeAverage(user, totalTime) >= 33)
              ? charts.MaterialPalette.white
              : charts.MaterialPalette.yellow.shadeDefault.darker;
          return new charts.TextStyleSpec(color: color);
        },
      )
    ];
    engagementScore = computeScore(usersActiveTimeMap.values.toList());
    if (double.parse(engagementScore) <= 33) {
      engagementColor = Colors.red;
      engagementString = 'Unbalanced engagment';
    }
    busy = false;
    return transcript;
  }

  double computeAverage(ActiveUsers user, Duration totalTime) =>
      ((user.activeTime * 100).inSeconds.toInt() / totalTime.inSeconds.toInt())
          .truncateToDouble();

  String computeScore(List<Duration> times) {
    var seconds = times.map((time) => time.inSeconds.toInt()).toList();
    var total = seconds.reduce((m, n) => m + n);
    var maxDisplacement = total * 1.6;
    var average = total / seconds.length;
    var displacements = seconds.map((time) => (time - average).abs()).toList();
    var sum = displacements.reduce((m, n) => m + n);
    var badPercent = (sum / maxDisplacement) * 100;
    return (100 - badPercent).toStringAsFixed(2);
  }

  static Future<String> loadAsset(String name) async {
    return rootBundle.loadString('$name');
  }

  static Map<String, dynamic> fromStringToJsonMap(String value) {
    return json.decode(value);
  }

  static List fromStringToJsonList(String value) {
    return json.decode(value);
  }
}
