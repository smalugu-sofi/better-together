// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Timeline _$TimelineFromJson(Map<String, dynamic> json) {
  return Timeline(
    timeline: (json['timeline'] as List<dynamic>)
        .map((e) => TimelineElement.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$TimelineToJson(Timeline instance) => <String, dynamic>{
      'timeline': instance.timeline,
    };

TimelineElement _$TimelineElementFromJson(Map<String, dynamic> json) {
  return TimelineElement(
    ts: json['ts'] as String,
    activeUsers: (json['activeUsers'] as List<dynamic>)
        .map((e) => ActiveUsers.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$TimelineElementToJson(TimelineElement instance) =>
    <String, dynamic>{
      'ts': instance.ts,
      'activeUsers': instance.activeUsers,
    };

ActiveUsers _$ActiveUsersFromJson(Map<String, dynamic> json) {
  return ActiveUsers(
    avatarUrl: json['avatarUrl'] as String,
    userId: json['userId'] as int,
    username: json['username'] as String,
    activeTime: Duration(microseconds: json['activeTime'] as int),
  );
}

Map<String, dynamic> _$ActiveUsersToJson(ActiveUsers instance) =>
    <String, dynamic>{
      'avatarUrl': instance.avatarUrl,
      'userId': instance.userId,
      'username': instance.username,
      'activeTime': instance.activeTime.inMicroseconds,
    };
