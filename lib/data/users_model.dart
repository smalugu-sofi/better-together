import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';

part 'users_model.g.dart';

@JsonSerializable()
class UsersInfo {
  UsersInfo({
    required this.users,
  });

  List<User> users;

  factory UsersInfo.fromRawJson(String str) =>
      UsersInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UsersInfo.fromJson(Map<String, dynamic> json) => UsersInfo(
        users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "users": List<dynamic>.from(users.map((x) => x.toJson())),
      };
}

class User {
  User({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.type,
    required this.picUrl,
  });

  String id;
  String firstName;
  String lastName;
  String email;
  int type;
  String picUrl;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        type: json["type"],
        picUrl: json["pic_url"] == null ? null : json["pic_url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "type": type,
        "pic_url": picUrl == null ? null : picUrl,
      };
}
