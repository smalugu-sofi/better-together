// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UsersInfo _$UsersInfoFromJson(Map<String, dynamic> json) {
  return UsersInfo(
    users: (json['users'] as List<dynamic>)
        .map((e) => User.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UsersInfoToJson(UsersInfo instance) => <String, dynamic>{
      'users': instance.users,
    };
